# CINES Spack Environments Templates

Complementary files for: [CINES Spack Environment Documentation](https://dci.dci-gitlab.cines.fr/webextranet/user_support/index.html#installing-software-using-spack-on-adastra)

## Purpose

The Spack environment files provided serve to facilitate CINES users in configuring their development environment at CINES. The base environment file encompasses the compilers and packages installed at the system level.

For further reference, please consult the Spack documentation available [here](https://spack.readthedocs.io/en/latest/).